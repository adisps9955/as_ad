(function f() {
    var url = "{$url}"
    var src = `{$src}`


    function p(str, sp, i) {
        if (sp.includes(sp)) {
            try {
                var r = str.split(sp)[i]
                if (r) return r
            } catch (Err) {}
        }
        return str
    }


    var dataJson = "[{"

    if (url.toLowerCase().includes("neonime.")) {
        // Neonime info anime get all eps
        var newBody = p(p(src, "episodios", 1), "</ul></div></div></div>", 0)


        var index = 1
        var epsDataa = newBody.split("<li>")

        var epsData = new Array;
        for (var i = epsDataa.length - 1; i >= 0; i--) {
            epsData.push(epsDataa[i])
        }


        for (var i = 0; i < epsData.length; i++) {
            var edata = epsData[i]
            if (edata.length < 1 || !edata.includes("</li>") || !edata.includes("numerando") || !edata.includes("href=")) continue

            var link = p(p(edata, "href=", 1), '"', 1)

            var date = p(p(edata, 'class="date">', 1), "</span>", 0)

            var dataEpsJs = `"episode": ${index}, "url": "${link}", "date": "${date}"`
            if (dataJson.length > 5) dataJson += "},{"
            dataJson += dataEpsJs

            index++
        }
        //console.log("Neonime: ")
    }

    if (url.toLowerCase().includes("otakudesu.")) {
        // OtakuDesu anime info get all eps
        var newBody = p(p(src, "episodelist", 2), "</ul></div><div", 0)

        var index = 1
        var epsDataa = newBody.split("<li>")

        var epsData = new Array;
        for (var i = epsDataa.length - 1; i >= 0; i--) {
            epsData.push(epsDataa[i])
        }


        for (var i = 0; i < epsData.length; i++) {
            var edata = epsData[i]
            if (edata.length < 1 || !edata.includes("</li>") || !edata.includes("<span>") || !edata.includes("href=")) continue

            var link = p(p(edata, "href=", 1), '"', 1)

            var date = edata.substring(0, edata.lastIndexOf("</span>"))
            date = date.substring(date.lastIndexOf(">") + 1)

            var prefix = p(p(edata, "</span>", 0), "</a>", 0)
            prefix = prefix.substring(prefix.lastIndexOf(">") + 1)
            prefix = prefix.toLowerCase()
            prefix = ("E" + prefix.substring(prefix.lastIndexOf("episode") + 1)).replace("subtitle", "").replace("indonesia", "").replace("  ", "").replace("&#8211;", "-")
            if (prefix.includes("==")) prefix = p(prefix, "==", 0)

            var dataEpsJs = `"episode": ${index}, "url": "${link}", "date": "${date}", "prefix": "${prefix}"`
            if (dataJson.length > 5) dataJson += "},{"
            dataJson += dataEpsJs

            index++
        }

        //console.log("Otakudesu: ")
    }

    if (url.toLowerCase().includes("animasu.")) {
        var newBody = p(p(src, 'id="daftarepisode"', 1), "</ul><div ", 0)

        var index = 1
        var epsDataa = newBody.split("<li>")

        var epsData = new Array;
        for (var i = epsDataa.length - 1; i >= 0; i--) {
            epsData.push(epsDataa[i])
        }


        for (var i = 0; i < epsData.length; i++) {
            var edata = epsData[i]
            if (edata.length < 1 || !edata.includes("</li>") || !edata.includes("<a") || !edata.includes("href=")) continue

            var link = p(p(edata, "href=", 1), '"', 1)

            var date = ""

            var dataEpsJs = `"episode": ${index}, "url": "${link}", "date": "${date}"`
            if (dataJson.length > 5) dataJson += "},{"
            dataJson += dataEpsJs

            index++
        }

        //console.log("Animasu: ")
    }

    if (url.toLowerCase().includes("riie.")) {
        var newBody = p(src, 'id="episodes-list"', 1)

        var index = 1
        var epsDataa = newBody.split("<li class")

        var epsData = new Array;
        for (var i = epsDataa.length - 1; i >= 0; i--) {
            epsData.push(epsDataa[i])
        }


        for (var i = 0; i < epsData.length; i++) {
            var edata = epsData[i]
            if (edata.length < 1 || !edata.includes("</li>") || !edata.includes("<a") || !edata.includes("href=")) continue

            var link = p(p(edata, "href=", 1), '"', 1)

            var date = ""

            var dataEpsJs = `"episode": ${index}, "url": "${link}", "date": "${date}"`
            if (dataJson.length > 5) dataJson += "},{"
            dataJson += dataEpsJs

            index++
        }

        //console.log("Riie: ")
    }


    if (dataJson.length > 5)
        dataJson += "}]";
    else
        dataJson = "";
    //console.log(dataJson)
    return dataJson

}());
