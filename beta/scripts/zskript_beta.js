(function f() {
    var url = "{$url}"
    var src = `{$src}`

    var getLocation = function(href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
    var l = getLocation(url);

    function p(str, sp, i) {
        if (sp.includes(sp)) {
            try {
                var r = str.split(sp)[i]
                if (r) return r
            } catch (Err) {}
        }
        return str
    }

    var jsScripts = src.split('type="text/javascript"')
    for (var i = 0; i < jsScripts.length; i++) {
        var tjs = jsScripts[i]
        if (!tjs.includes("'dlbutton'") || !tjs.includes("</script>")) continue
        var script = p(tjs, "</script>", 0)

        var link = eval(p(p(script, ").href =", 1), ";", 0))
        console.log(link)
        return "https://" + l.hostname + link
    }
    return ""
}())
